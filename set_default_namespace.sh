#!/bin/bash

mkdir -p $(dirname ~/.kube/config)
curl -Lo ~/.kube/config $KUBECONFIG_URL
account_alias=$(aws iam list-account-aliases | jq '.AccountAliases[0]' -r)
sub="dfds-"
default_k8s_ns=${account_alias/$sub/""}
kubectl config set-context --current --namespace=$default_k8s_ns