# ========================================
# CREATE UPDATED BASE IMAGE
# ========================================

FROM debian:stretch-slim AS base

RUN apt-get update \
    && apt-get dist-upgrade -y \
    && apt-get clean \
    && rm -rf /var/lib/apt/lists/*


# ========================================
# GENERAL PREREQUISITES
# ========================================

FROM base

# Explicitly set USER env variable to accomodate issues with golang code being cross-compiled
ENV USER root

RUN apt-get update \
    && apt-get install -y curl unzip git bash-completion jq ssh \
    && apt-get clean \
    && rm -rf /var/lib/apt/lists/*

# Adding  GitHub public SSH key to known hosts
RUN ssh -T -o "StrictHostKeyChecking no" -o "PubkeyAuthentication no" git@github.com || true

# ========================================
# PYTHON
# ========================================

RUN apt-get update \
    && apt-get install -y python python-pip python3 python3-pip \
    && apt-get clean \
    && rm -rf /var/lib/apt/lists/*

# ========================================
# AWS CLI
# ========================================

# Always install newest version
# Doesn't seem to allow version lock https://docs.aws.amazon.com/cli/latest/userguide/install-cliv2-linux.html

# RUN curl "https://awscli.amazonaws.com/awscli-exe-linux-x86_64.zip" -o "awscliv2.zip" \
#     && unzip awscliv2.zip \
#     && ./aws/install \
#     && rm -rf ./aws \
#     && rm awscliv2.zip

# ENV AWS_PAGER=""

ENV AWSCLI_VERSION=1.18.109

RUN python3 -m pip install --upgrade pip \
    && pip3 install pipenv awscli==${AWSCLI_VERSION} \
    && echo "complete -C '$(which aws_completer)' aws" >> ~/.bashrc


# ========================================
# AWS IAM AUTHENTICATOR
# ========================================

ENV VERSION=1.14.6
ENV BUILD_DATE=2019-08-22
ENV DOWNLOAD_URL=https://amazon-eks.s3-us-west-2.amazonaws.com/${VERSION}/${BUILD_DATE}/bin/linux/amd64/aws-iam-authenticator
ENV LOCAL_FILE=./aws-iam-authenticator
RUN curl -Lo $LOCAL_FILE $DOWNLOAD_URL
RUN chmod +x $LOCAL_FILE
RUN mv $LOCAL_FILE /usr/local/bin


# ========================================
# SAML2AWS
# ========================================

ENV SAML2AWS_VERSION=2.13.0

RUN curl -LO https://github.com/Versent/saml2aws/releases/download/v${SAML2AWS_VERSION}/saml2aws_${SAML2AWS_VERSION}_linux_amd64.tar.gz && \
	tar -xvzf saml2aws_${SAML2AWS_VERSION}_linux_amd64.tar.gz   \
    && mv saml2aws /usr/bin/saml2aws \
    && rm saml2aws_${SAML2AWS_VERSION}_linux_amd64.tar.gz


# ========================================
# TERRAFORM
# ========================================

ENV TERRAFORM_VERSION=0.12.25

RUN curl -L https://releases.hashicorp.com/terraform/${TERRAFORM_VERSION}/terraform_${TERRAFORM_VERSION}_linux_amd64.zip -o terraform.zip \
    && unzip terraform.zip \
    && rm terraform.zip \
    && mv terraform /usr/local/bin/
    #&& terraform -install-autocomplete


# ========================================
# TERRAGRUNT
# ========================================
ENV TERRAGRUNT_VERSION=0.23.10

RUN curl -L https://github.com/gruntwork-io/terragrunt/releases/download/v${TERRAGRUNT_VERSION}/terragrunt_linux_amd64 -o terragrunt \
    && chmod +x terragrunt \
    && mv terragrunt /usr/local/bin/


# ========================================
# KUBECTL
# ========================================

ENV KUBECTL_VERSION=1.17.0

RUN curl -L https://storage.googleapis.com/kubernetes-release/release/v${KUBECTL_VERSION}/bin/linux/amd64/kubectl -o kubectl \
    && chmod +x kubectl \
    && mv kubectl /usr/local/bin/

COPY init.sh /usr/bin/init.sh

RUN chmod +x /usr/bin/init.sh && init.sh

COPY auto_saml_login.sh /usr/bin/auto_saml_login.sh
RUN chmod +x /usr/bin/auto_saml_login.sh

COPY set_default_namespace.sh /usr/bin/set_default_namespace.sh

RUN chmod +x /usr/bin/set_default_namespace.sh

RUN echo '[ ! -z "$TERM" -a -r /etc/motd ] && cat /etc/issue && cat /etc/motd' \
    >> /etc/bash.bashrc \
    ; echo "\e[32m\
===================================================================\n\
| Capability tools Docker container                               |\n\
===================================================================\n\
\n\
Maintainer DFDS A/S\n\
\n\
The container image provides terraform and kubectl setup that allows you to access your capability account on AWS and K8s namespace\n\
You can login to your capability account using one of the two following approaches:\n\
\n\
A) Using your DFDS SAML login:\n\
    Instructions:\n\
        1) Login to AWS\n\
            Run this command: saml2aws login\n\
        2) \033[1mOPTIONAL:\033[0m \e[32mSet default k8s namespace to the selected AWS capability account\n\
            Run this command: set_default_namespace.sh\n\
            \033[1mNote:\033[0m \e[32m You need to provide the environment variable KUBECONFIG_URL\n\
\n\
B) In case if AD Service User and IAM Role are already provided, then do the followings instead:\n\
    Instructions:\n\
        1) Login to AWS\n\
            Run this command: auto_saml_login.sh\n\
        2) \033[1mOPTIONAL:\033[0m \e[32mSet default k8s namespace to the selected AWS capability account\n\
            Run this command: set_default_namespace.sh\n\
            \033[1mNote:\033[0m \e[32m You need to provide the environment variable KUBECONFIG_URL\n\
\n\
\e[0m\n"\
    > /etc/motd

CMD [ "bash" ]