# docker-terraform-terragrunt

Image with [Terrragrunt](https://github.com/gruntwork-io/terragrunt) executables

# Running 

## Using a DFDS user (SAML2AWS)

Pull image:
```
docker pull registry.gitlab.com/dfds-platform/docker-terraform-terragrunt:xxxx
```

Run container:
```
docker run -it -v C:\Repos\yourproject\:/project -e AWS_PROFILE=saml -w="/project" --entrypoint "/bin/bash" registry.gitlab.com/dfds-platform/docker-terraform-terragrunt:xxxx
```

Login to a capability account:
```
saml2aws login 
```

## Using AWS Acess and Secret
The access key and secret for the AWS capability account are stored in parameter store in the capability account. They should be used only for running this docker image through pipeline.

```
docker run -it -v C:\Repos\yourproject\:/project -e AWS_ACCESS_KEY_ID=xxxxxxxxxxxx -e AWS_SECRET_ACCESS_KEY=xxxxxxxxxxxx -w="/project" --entrypoint "/bin/bash" registry.gitlab.com/dfds-platform/docker-terraform-terragrunt:xxxx
```

## Image tag version
Check [here](https://gitlab.com/dfds-platform/docker-terraform-terragrunt/container_registry) for latest image version. You can also check [Release section](https://gitlab.com/dfds-platform/docker-terraform-terragrunt/-/releases) for details.

# Contribution (Not working)
- Create a pull request and wait until it has been validated by automated build & test flow. The pipeline used for this is named: docker-terraform-terragrunt-pull-req
- Once it has been validated then merge the pull request
- Create a new Git tag in history
- The pipeline docker-terraform-terragrunt-cd should now start building and publishing a new docker image