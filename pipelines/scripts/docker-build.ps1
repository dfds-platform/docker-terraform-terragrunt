param(        
    [string] $dockerFile = $null,
    [string] $imageName = $null
)

$ErrorActionPreference = "Stop"

write-host "Building docker image..." -foregroundcolor green
docker build . --file $dockerFile -t $imageName
if ($LASTEXITCODE -ne 0) { exit $LASTEXITCODE }