param(
    [string] $imageName = $null
)

$ErrorActionPreference = "Stop"

## Test Terraform setup
write-host "Validating Terraform version..." -foregroundcolor green
$containerNameTerraform = "test-terraform-container"
docker run --name $containerNameTerraform "${imageName}:latest" terraform -v
if ($LASTEXITCODE -ne 0) { exit $LASTEXITCODE }
$containerNameTerraformOut=docker logs $containerNameTerraform
if(!$containerNameTerraformOut[0].StartsWith("Terraform v0.12")){
    Write-Host "Failed."
}
docker rm $containerNameTerraform
if ($LASTEXITCODE -ne 0) { exit $LASTEXITCODE }
write-host "Validation Terraform version. Succeeded." -foregroundcolor green


## Test Terragrunt setup
write-host "Validating Terragrunt installation..." -foregroundcolor green
$containerNameTerragrunt = "test-terragrunt-container"
docker run --name $containerNameTerragrunt "${imageName}:latest" terraform -v
if ($LASTEXITCODE -ne 0) { exit $LASTEXITCODE }
$containerNameTerragruntOut=docker logs $containerNameTerragrunt
if(!$containerNameTerragruntOut.StartsWith("Terragrunt")){
    Write-Host "Failed."
}
docker rm $containerNameTerragrunt
if ($LASTEXITCODE -ne 0) { exit $LASTEXITCODE }
write-host "Validation Terragrunt installation. Succeeded." -foregroundcolor green