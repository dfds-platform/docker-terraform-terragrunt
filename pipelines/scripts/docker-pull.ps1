param(        
    $buildNumber = $null,
    [string] $imageRepo = $null,
    [string] $imageName = $null,    
    $awsRegistryId = $null
)


$ErrorActionPreference = "Stop"

# exit if push image is requested but a build number has not been specified at the command line
if ($null -eq $buildNumber) {
    throw "Cannot pull container image without a build number. Specify one at the command line."
}

Write-Host 'Checking if AWS CLI is installed...'
if (!(Get-Command "aws" -ErrorAction SilentlyContinue))
{
    Throw 'AWS cli is not installed. Install AWS cli and run the pipeline again'
}
else
{
    Write-Host "AWS already installed."
} 

write-host "Getting AWS login command for docker..." -foregroundcolor green
$dockerlogincmd = aws ecr get-login --no-include-email --region eu-central-1  --registry-ids $awsRegistryId
if ($LASTEXITCODE -ne 0) { exit $LASTEXITCODE }

write-host "Login to docker..." -foregroundcolor green
Invoke-Expression -Command $dockerlogincmd
if ($LASTEXITCODE -ne 0) { exit $LASTEXITCODE }

write-host "Pulling image from ECR with tag number $buildNumber..." -foregroundcolor green
docker pull "${imageRepo}:${buildNumber}"
if ($LASTEXITCODE -ne 0) { exit $LASTEXITCODE }